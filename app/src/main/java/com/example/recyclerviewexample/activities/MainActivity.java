package com.example.recyclerviewexample.activities;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.widget.Toast;

import com.example.recyclerviewexample.R;
import com.example.recyclerviewexample.adapters.MyAdapter;
import com.example.recyclerviewexample.models.Anime;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    private List<Anime> animeList;
    private RecyclerView recyclerView;
    private RecyclerView.Adapter adapter;
    private RecyclerView.LayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        animeList = this.getAllAnimes();

        recyclerView = findViewById(R.id.recycler);
        layoutManager = new LinearLayoutManager(this);

        adapter = new MyAdapter(animeList, R.layout.item_list, new MyAdapter.OnItemClickListener() {
            @Override
            public void onItemClick(String name, int position) {
                Toast.makeText(MainActivity.this, name + " - " + position, Toast.LENGTH_SHORT).show();
            }
        });

        recyclerView.setLayoutManager(layoutManager);
        recyclerView.setAdapter(adapter);
    }

    private List<Anime> getAllAnimes(){
        return new ArrayList<Anime>() {{
            add(new Anime("One Piece", "acción, aventuras", "shonen", 1997));
            add(new Anime("Berserk", "acción, fantasía oscura, gore", "seinen", 1989));
            add(new Anime("Pokémon", "aventuras, maltrato animal", "kodomo", 1997));
            add(new Anime("Sailor Moon", "acción, romance, magical girls", "shoujo", 1997));
            add(new Anime("Slam Dunk", "spokon, comedia", "shonen", 1990));
        }};
    }
}