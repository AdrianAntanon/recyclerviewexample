package com.example.recyclerviewexample.adapters;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.example.recyclerviewexample.R;
import com.example.recyclerviewexample.models.Anime;

import java.util.List;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.ViewHolder> {

    private List<Anime> animeArrayList;
    private int layout;
    private OnItemClickListener itemClickListener;

    public MyAdapter(List<Anime> animeArrayList, int layout, OnItemClickListener listener) {
        this.animeArrayList = animeArrayList;
        this.layout = layout;
        this.itemClickListener = listener;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(parent.getContext()).inflate(this.layout, parent, false);
//        ViewHolder viewHolder = new ViewHolder(view);
//        return viewHolder; //PUEDO HACER ESTO O DIRECTAMENTE LO DE ABAJO
        return new ViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        holder.bind(this.animeArrayList.get(position), this.itemClickListener);
    }

    @Override
    public int getItemCount() {
        return this.animeArrayList.size()   ;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        public TextView textViewName;

        public ViewHolder(@NonNull View itemView) {
            super(itemView);
            this.textViewName = itemView.findViewById(R.id.itemId);
        }

        public void bind(final Anime anime, final OnItemClickListener itemClickListener) {
            this.textViewName.setText(anime.getTitle());
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    itemClickListener.onItemClick(anime.getInfoAnime(), getAdapterPosition());
                }
            });
        }
    }

    public interface OnItemClickListener{
        void onItemClick(String name, int position);
    }
}
