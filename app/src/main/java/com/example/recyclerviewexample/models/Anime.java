package com.example.recyclerviewexample.models;

import com.example.recyclerviewexample.R;

public class Anime {
    private String title, category, genre;
    private int image, originalRun;

    public Anime(String title, String category, String genre, int originalRun) {
        this.title = title;
        this.category = category;
        this.genre = genre;
        this.originalRun = originalRun;
        setImage();
    }

    public void setImage() {
        String categorySelected = genre.toLowerCase();

        switch (categorySelected) {
            case "shonen":
            case "shounen":
            case "shojo":
            case "shoujo":
            case "seinen":
            case "kodomo":
                this.image = R.mipmap.ic_example;
                break;
        }
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public void setGenre(String genre) {
        this.genre = genre;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public void setOriginalRun(int originalRun) {
        this.originalRun = originalRun;
    }

    public String getInfoAnime(){

        return "Título: "+ getTitle() + "\n"
                +"Categoría: " + getCategory() + "\n"
                +"Género: " + getGenre() + "\n"
                +"Año de estreno: " + getOriginalRun();

    }

    public String getTitle() {
        return title;
    }

    public String getCategory() {
        return category;
    }

    public String getGenre() {
        return genre;
    }

    public int getImage() {
        return image;
    }

    public int getOriginalRun() {
        return originalRun;
    }
}
